#include "cheetah/data/test_utils/TimeSeriesTest.h"
#include "cheetah/data/test_utils/TimeSeriesTester.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace data {
namespace test {

typedef ::testing::Types<
    TimeSeriesTestTraits<cheetah::Cuda,float>,
    TimeSeriesTestTraits<cheetah::Cuda,double>,
    TimeSeriesTestTraits<cheetah::Cuda,char>,
    TimeSeriesTestTraits<cheetah::Cuda,uint8_t>
    > CudaTimeSeriesTestTypes;
TYPED_TEST_CASE(TimeSeriesTest, CudaTimeSeriesTestTypes);

TYPED_TEST(TimeSeriesTest, test_samples)
{
    SampleCountTest<TypeParam>::test(1000L);
    SampleCountTest<TypeParam>::test(1<<23);
}

template<typename ValueType>
class CudaTimeSeriesTesterTraits : public TimeSeriesTesterTraits<data::TimeSeries<cheetah::Cuda, ValueType>>
{
    typedef TimeSeriesTesterTraits<data::TimeSeries<cheetah::Cuda, ValueType>> BaseT;
    typedef typename BaseT::Allocator Allocator;
};


typedef ::testing::Types<CudaTimeSeriesTesterTraits<float>, CudaTimeSeriesTesterTraits<uint8_t>> CudaTimeSeriesTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(BasicCudaSeries, TimeSeriesTester, CudaTimeSeriesTraitsTypes);



} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
