/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/ddtr/gpu_bruteforce/DedispersionStrategy.h"
#include "cheetah/ddtr/gpu_bruteforce/detail/kernels/gpu_brute_force_kernel.cu"
#include "cheetah/ddtr/gpu_bruteforce/detail/kernels/batched_gpu_bin.cu"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace gpu_bruteforce {

template<typename NumericalT>
void Kernel<NumericalT>::bin_gpu( data::FrequencyTime<Cuda, NumericalT>& d_input
                                , int nelements
                                , int bin
                                , const int threads_per_block
                                )
{
    int binned_elements = (nelements/(bin/2));
    panda::nvidia::CudaDevicePointer<NumericalT> d_temp(threads_per_block);
    int number_of_blocks = std::ceil((float)(binned_elements)/(float)(2*threads_per_block));

    for(int block=0; block<number_of_blocks; ++block)
    {
        batched_gpu_bin<<<1, threads_per_block>>>( &(*d_input.begin())
                                                 , &(*d_temp.begin())
                                                 , block
                                                 , threads_per_block
                                                 , binned_elements);
    }
}

template<typename NumericalT>
void Kernel<NumericalT>::exec(int dm_range
                             , DedispersionStrategy<Cpu, NumericalT> const& strategy
                             , data::FrequencyTime<Cuda, NumericalT> const& d_input
                             , float* d_output
                             , int dsamps
                             , int bin
                             , const int threads_per_block
                             )
{
    cudaMemcpyToSymbol(dmshifts, &*(strategy.dmshifts().begin()), strategy.nchans() * sizeof(float));

    gpu_brute_force_kernel<<<std::ceil(dsamps/(float)threads_per_block), threads_per_block>>>
                                                            ( &(*d_input.begin())
                                                            , d_output
                                                            , (int)d_input.number_of_channels()
                                                            , dsamps/bin
                                                            , (int)d_input.number_of_spectra()/bin
                                                            , strategy.dm_low()[dm_range].value()
                                                            , strategy.dm_step()[dm_range].value()
                                                            , (int)strategy.ndms()[dm_range]
                                                            , bin);
}

} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace cheetah
} // namespace ska
