/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Log.h"


namespace ska {
namespace cheetah {
namespace ddtr {
namespace cpu {


template<typename DdtrTraits>
Worker<DdtrTraits>::Worker()
{
}

template<typename DdtrTraits>
std::shared_ptr<typename Worker<DdtrTraits>::DmTrialsType> Worker<DdtrTraits>::operator()(BufferType const& buffer, std::shared_ptr<DedispersionPlan<DdtrTraits>> plan)
{
    auto const& data = buffer.buffer();
    std::size_t nchans = data.number_of_channels();

    auto dm_trial_metadata = plan->dm_trials_metadata(data.metadata(), data.template dimension<data::Time>());

    std::shared_ptr<DmTrialsType> dmtrials_ptr = DmTrialsType::make_shared(dm_trial_metadata, data.start_time());
    if (data.data_size() < plan->buffer_overlap() * nchans)
    {
        PANDA_LOG_WARN << "AggregationBuffer is too small to be processed ("
                       << data.data_size() << " < " << plan->buffer_overlap()*nchans << ")\n"<<"Skipping Current Buffer";
        return dmtrials_ptr;
    }

    DmTrialsType& dmtrials = *(dmtrials_ptr);
    NumericalT const* tf_data = &*data.begin();

    auto const& plan_dm_trials = plan->dm_trials();
    auto const& plan_dm_factors = plan->dm_factors();
    for (std::size_t dm_idx = 0; dm_idx < plan->dm_trials().size(); ++dm_idx)
    {
        auto& current_trial = dmtrials[dm_idx];
        auto const& plan_dm_trial = plan_dm_trials[dm_idx].value();
        for (std::size_t samp_idx=0; samp_idx < current_trial.size(); ++samp_idx)
        {
            float sum = 0.0f;
            for (std::size_t chan_idx=0; chan_idx < nchans; ++chan_idx)
            {
                std::size_t delay = static_cast<std::size_t>(plan_dm_factors[chan_idx] * plan_dm_trial);
                std::size_t input_idx = (samp_idx + delay) * nchans + chan_idx;
                if (input_idx >= data.data_size())
                {
                    PANDA_LOG_ERROR << "Input index beyond end of buffer (" << input_idx << " >= " << data.data_size() << ")";
                }
                sum += (float) tf_data[input_idx];
            }
            current_trial[samp_idx] = sum/nchans;
        }
    }

    return dmtrials_ptr;
}

} // namespace cpu
} // namespace ddtr
} // namespace cheetah
} // namespace ska
