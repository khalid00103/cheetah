#include "cheetah/dred/cuda/Dred.cuh"
#include "cheetah/cuda_utils/cuda_thrust.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/Units.h"

namespace ska {
namespace cheetah {
namespace dred {
namespace cuda {

template <typename T>
Dred<T>::Dred(Config const& config, dred::Config const& algo_config)
    : utils::AlgorithmBase<Config,dred::Config>(config, algo_config)
    , _pwft(_algo_config.pwft_config())
{
}

template <typename T>
Dred<T>::~Dred()
{
}

template <typename T>
void Dred<T>::median_scrunch5(const SeriesType& in, SeriesType& out)
{
    size_t count = in.size();
    const T* ptr = thrust::raw_pointer_cast(in.data());
    if( count == 1 )
    {
        out[0] = in[0];
    }
    else if( count == 2 )
    {
        out[0] = 0.5f*(in[0] + in[1]);
    }
    else if( count == 3 )
    {
        out[0] = detail::median3<T>(in[0],in[1],in[2]);
    }
    else if( count == 4 )
    {
        out[0] = detail::median4<T>(in[0],in[1],in[2],in[3]);
    }
    else
    {
        thrust::transform(thrust::cuda::par,
            thrust::make_counting_iterator<unsigned int>(0),
            thrust::make_counting_iterator<unsigned int>(in.size()/5),
            out.begin(), detail::Median5Functor<T>(ptr));
    }
}

template <typename T>
void Dred<T>::linear_stretch(const SeriesType& in, SeriesType& out, float step)
{
    T const* ptr = thrust::raw_pointer_cast(in.data());
    thrust::transform(thrust::cuda::par,
        thrust::make_counting_iterator<unsigned int>(0),
        thrust::make_counting_iterator<unsigned int>(out.size()),
        out.begin(), detail::LinearStretchFunctor<T>(ptr, in.size(), step));
}

template <typename T>
template <typename Alloc>
void Dred<T>::prepare(data::FrequencySeries<cheetah::Cuda, ComplexType, Alloc> const& input,
    data::FrequencySeries<cheetah::Cuda, ComplexType, Alloc>& output,
    data::AccelerationType maximum_acceleration)
{
    output.resize(input.size());
    output.frequency_step(input.frequency_step());

    //observation length in seconds
    float tobs = 1/input.frequency_step().value();
    //nyquist frequency in hz
    float nyquist = input.frequency_step().value() * input.size();
    //factor by which median window should be bigger than the Z value at a given frequency
    std::size_t k = _algo_config.oversmoothing_factor();
    //The power of 5 to smooth by
    std::size_t power = 1;

    //clear the vector of boundaries and windows
    _boundaries.clear();

    //Get the acceleration value that will determine the smoothing windows
    auto accel_max = maximum_acceleration.value();

    //calculate the boundaries
    //this will be done for every execution but should incur only minor performance penalties
    while (true) {
        Boundary boundary;
        boundary.window = std::pow((std::size_t)5,(std::size_t)power);
        auto c = boost::units::si::constants::codata::c.value().value();
        boundary.frequency = (c * boundary.window) / (k * accel_max * tobs * tobs);
        boundary.idx = std::min((std::size_t) (boundary.frequency/input.frequency_step().value()), input.size());
        _boundaries.push_back(boundary);
        if (boundary.frequency>nyquist)
            break;
        power+=1;
    }

    //This array will only be resized if the number of boundaries
    //has changed since the last execution
    _medians.resize(_boundaries.size());
    for (int ii=0; ii<_boundaries.size(); ++ii)
    {
        //similarly these array will only be resized if either the input
        //size or the actual boundaries have changed.
        _medians[ii].resize(input.size()/_boundaries[ii].window);
    }
}


template <typename T>
template <typename Alloc>
void Dred<T>::process(ResourceType& gpu,
    data::FrequencySeries<cheetah::Cuda, ComplexType, Alloc> const& input,
    data::FrequencySeries<cheetah::Cuda, ComplexType, Alloc>& output,
    data::AccelerationType maximum_acceleration)
{
    prepare(input, output, maximum_acceleration);
    SeriesType power_spectrum(input.size());
    power_spectrum.frequency_step(input.frequency_step());
    SeriesType intermediate(input.size());
    SeriesType baseline(input.size());

    _pwft.process_direct(gpu,input, power_spectrum);
    SeriesType* in = &(power_spectrum);
    SeriesType* out;
    std::size_t offset = 0;
    for (int ii=0;ii<_boundaries.size();ii++)
    {
        out = &(_medians)[ii];
        median_scrunch5(*in, *out);
        linear_stretch(*out, intermediate, (float)(_boundaries[ii].window));
        thrust::copy_n(intermediate.begin()+offset,
            _boundaries[ii].idx-offset,
            baseline.begin()+offset);
        offset = _boundaries[ii].idx;
        in = out;
    }
    thrust::transform(thrust::cuda::par, input.begin(),input.end(),
        baseline.begin(),output.begin(),detail::PowerNormalisingFunctor<T>());
    CUDA_ERROR_CHECK(cudaDeviceSynchronize());
}

} //cuda
} //dred
} //cheetah
} //ska
