/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DRED_DRED_H
#define SKA_CHEETAH_DRED_DRED_H

#include "cheetah/dred/Config.h"
#include "cheetah/dred/cuda/Dred.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/utils/Architectures.h"

#include "panda/AlgorithmTuple.h"

namespace ska {
namespace cheetah {
namespace dred {


/**
 * @brief      Class for implementing spectral dereddening
 *
 * @tparam     T     The base value type of the data to be dereddened
 */
template <typename T>
class Dred
{
    public:
        typedef panda::AlgorithmTuple<cuda::Dred<T>> Implementations;

    public:
        /**
         * @brief      Construct a new Dred instance
         *
         * @param      config  A Dred configuration object
         */
        Dred(Config const& config);
        Dred(Dred const&) = delete;
        Dred(Dred&&) = default;
        ~Dred();

        /**
         * @brief      Deredden a complex frequency series
         *
         * @details     This method forwards to the relevant implementation based on the Arch parameter
         *             and the types of the arguments passed.
         *
         * @param      resource              The resource to process on
         * @param      input                 The frequency series to be dereddened
         * @param      output                The output for the dereddened frequency series
         * @param[in]  maximum_acceleration  The maximum acceleration
         *                                   (use to calculate the median filter window size
         *                                    at each frequency)
         * @param[in]  args                  Additional arguments to be passed to implementation
         *
         * @tparam     Arch                  The architectiure to be processed on
         * @tparam     Alloc                 The allocator type of the input and output
         * @tparam     Args                  The types of any additional arguments
         */
        template <typename Arch, typename Alloc, typename... Args>
        void process(panda::PoolResource<Arch>& resource,
            data::FrequencySeries<Arch, typename data::ComplexTypeTraits<Arch,T>::type, Alloc>const& input,
            data::FrequencySeries<Arch, typename data::ComplexTypeTraits<Arch,T>::type, Alloc>& output,
            data::AccelerationType maximum_acceleration,
            Args&&... args);

    private:
        Config const& _config;
        Implementations _implementations;
};

} // namespace dred
} // namespace cheetah
} // namespace ska

#include "cheetah/dred/detail/Dred.cpp"

#endif // SKA_CHEETAH_DRED_DRED_H
