subpackage(cuda)

set(MODULE_BRDZ_LIB_SRC_CUDA
    ${LIB_SRC_CUDA}
    PARENT_SCOPE
)

set(MODULE_BRDZ_LIB_SRC_CPU
    src/Brdz.cpp
    src/Config.cpp
    ${LIB_SRC_CPU}
    PARENT_SCOPE
)

test_utils()
add_subdirectory(test)
