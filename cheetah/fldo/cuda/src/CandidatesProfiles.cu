
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/cuda/detail/FldoUtils.h"


namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
namespace util {

/*
 * void build_scrunched_profiles(size_t ncandidates, size_t max_phases,
 *                              size_t nsubbands, size_t nsubints, float mean, float *d_folded, float *d_weight,
 *                              float *d_outfprof, float* d_outprof, std::vector<util::GpuStream>&exec_stream)
 *
 * @brief Normalizes the profiles produced by the folding algorithm and
 * produces the reduced profiles for each pulsar candidate.
 *
 * @param ncandidates       the number of pulsar candidates
 * @param max_phases        the default max number of phases (128 or 256)
 * @param nsubbands         the number of frequencies sub-bands
 * @param nsubints          the number of sub-integration in time
 * @param mean              the input data mean value
 * @param d_folded          device memeory with folded data for all candidates
 * @param d_weight          device memory withwieghts of phases
 * @param d_outfprof        device memory to store profiles scrunched in freq
 * @param d_outprof         device memory to store profile scrunched in freq and time
 * @param exec_stream       an array of gpu streams to run kernels concurrently
 *
 * @return On failure throws a runtime_error exception.
 */
void build_scrunched_profiles(size_t ncandidates, size_t max_phases,
                              size_t nsubbands, size_t nsubints, float mean, float *d_folded, float *d_weight,
                              float *d_outfprof, float* d_outprof, std::vector<util::GpuStream>&exec_stream)
{
    dim3 threadsPerBlock;       //number of threads for each block
    dim3 blocksPerGrid;         //number of blocks for each grid
    // calculdate the gpu shared memory size for the default max number of phases
    int shared_memsize = max_phases * sizeof(float);
    PANDA_LOG_DEBUG << "Calling kernel to build profiles: threads= ("
                    << threadsPerBlock.x
                    << ", "
                    << threadsPerBlock.y
                    << ") blocks= ("
                    <<  blocksPerGrid.x
                    << ", "
                    << blocksPerGrid.y
                    << ")";
    PANDA_LOG_DEBUG << "raw time series mean: " << mean;
    //loop on candidates: each candidate handled by a different CUDA stream
    for (size_t ncand = 0; ncand < ncandidates; ++ncand) {
        //associate a gpu stream to each candidate kernel
        int nstream = ncand % exec_stream.size();
        // launch normalization kernel
        threadsPerBlock.x = max_phases; // one thread for each bin (phase)
        threadsPerBlock.y = 1;
        blocksPerGrid.x = nsubints;    // one block for each sub-integration
        blocksPerGrid.y = 1;           // one block for each sub-integration
        normalize_kernel<<<blocksPerGrid, threadsPerBlock, 0, exec_stream[nstream].stream() >>>
        (d_folded,
         d_weight,
         ncand,
         mean/nsubints,
         nsubbands);
        //check for kernel errors
        CUDA_ERROR_CHECK(cudaGetLastError());
        //launch kernel for profile
        threadsPerBlock.x = max_phases; // one thread for each bin (phase)
        threadsPerBlock.y = 1;
        blocksPerGrid.x = nsubints;     // one block for each sub-integration
        blocksPerGrid.y = 1;            // one block for each sub-integration
        profile_kernel<<< blocksPerGrid, threadsPerBlock, shared_memsize, exec_stream[nstream].stream() >>>
        (d_folded,
         d_outfprof,
         ncand,
         nsubbands);
        //check for kernel errors
        CUDA_ERROR_CHECK(cudaGetLastError());
        //reduce kernel for profile
        threadsPerBlock.x = max_phases;
        threadsPerBlock.y = 1;
        blocksPerGrid.x = 1;
        blocksPerGrid.y = 1;
        profile_kernel<<< blocksPerGrid, threadsPerBlock, shared_memsize, exec_stream[nstream].stream() >>>
        (d_outfprof,
         d_outprof,
         ncand,
         nsubints);
        //check for kernel errors
        CUDA_ERROR_CHECK(cudaGetLastError());
    }
}

} // util
} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska
