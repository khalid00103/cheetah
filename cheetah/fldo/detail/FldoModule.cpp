/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "NullFldo.h"
#include "cheetah/utils/TaskConfigurationSetter.h"

namespace ska {
namespace cheetah {
namespace fldo {

namespace {
    template<typename ConfigType>
    struct FldoFactory {
        public:
            FldoFactory(ConfigType const& config)
                : _config(config)
            {
            }

            template<typename Algo>
            Algo create()
            {
                return Algo(_config);
            }

            template<typename Algo>
            bool active() const {
                return _config.template config<typename Algo::Config>().active();
            }

        private:
            ConfigType const& _config;
    };

} // namespace

template<class FldoTraitsType, typename... FldoAlgos>
FldoModule<FldoTraitsType, FldoAlgos...>::FldoModule(Config const& config, Handler& handler)
    : _task(config.pool(), handler)
{
    FldoFactory<Config> factory(config);
    // setup the tasks object according to the configuration settings
    if(!utils::TaskConfigurationSetter<FldoAlgos...>::configure(_task, factory))
    {
        PANDA_LOG_WARN << "no FLDO algorithm has been specified";
        _task.set_algorithms(NullFldo<FldoTraitsType>());
    }
}

template<class FldoTraitsType, typename... FldoAlgos>
std::shared_ptr<panda::ResourceJob> FldoModule<FldoTraitsType, FldoAlgos...>::operator()(std::vector<std::shared_ptr<TimeFrequencyType>>& tf_data, data::Scl const& scl_data)
{
    return _task.submit(tf_data, scl_data);
}

} // namespace fldo
} // namespace cheetah
} // namespace ska
