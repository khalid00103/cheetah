/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_TDAO_H
#define SKA_CHEETAH_TDAO_H

#include "cheetah/tdao/Config.h"
#include "cheetah/tdao/cuda/Tdao.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/data/Ccl.h"

#include "panda/AlgorithmTuple.h"

namespace ska {
namespace cheetah {
namespace tdao {

/**
 * @brief Time Domain Spectral Peak Detection and Candidate List Output
 *
 * @details
 * TDAO will perform a search for signals of interest in the harmonic
 * spectra produced in HRMS and using the complex spectra from PWFT to
 * provide further information to help distinguish between real and
 * spurious signals.
 * TDAO will generate a list of candidate pulsars (CCL) which will
 * include all the metadata associated with the particular beam and also
 * include the DM, period, acceleration, detection statistic as a function
 * of DM and of the harmonic fold, and the statistic resulting from TDAO
 *
 */
class Tdao
{
    private:
        typedef panda::AlgorithmTuple<cuda::Tdao> Implementations;

    public:

        /**
         * @brief      Construct a new Tdao object
         *
         * @param      config  The algorithm configuration
         */
        Tdao(Config const& config);
        Tdao(Tdao const&) = delete;
        Tdao(Tdao&&) = default;
        ~Tdao();

        /**
         * @brief      Find significant peaks in a power series
         *
         * @details     This is a forwarding interface that will dispatch to the relevant
         *             implementation dependent on the input arguments.
         *
         * @param      resource    The resource to process on
         * @param      input       The power series to search for peaks in
         * @param      output      An output list of candidate peaks
         * @param      dm          The dispersion measure of the power series
         * @param      acc         The acceleration of the power series
         * @param[in]  nharmonics  The number of harmonic sums the power series has undergone
         * @param[in]  args        Any additional arguments to be passed to the implementation
         *
         * @tparam     Arch        The architecture to process on
         * @tparam     T           The value type of the power series
         * @tparam     Alloc       The allocator type of the power series
         * @tparam     Args        The types of any additional arguments to be passed to the implementation
         */
        template <typename Arch, typename T, typename Alloc, typename... Args>
        void process(panda::PoolResource<Arch>& resource,
            data::PowerSeries<Arch,T,Alloc> const& input,
            data::Ccl& output,
            data::DedispersionMeasureType<float>const& dm,
            data::AccelerationType const& acc,
            std::size_t nharmonics,
            Args&&... args);

    private:
        Config const& _config;
        Implementations _implementations;
};

} // namespace tdao
} // namespace cheetah
} // namespace ska

#include "cheetah/tdao/detail/Tdao.cpp"

#endif // SKA_CHEETAH_TDAO_H
