/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_COMMONPLAN_H
#define SKA_CHEETAH_MODULES_FFT_COMMONPLAN_H

#include "cheetah/modules/fft/Plan.h"
#include <vector>

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

/**
 * @brief A Plan that meets the requirements for all activated Algorithms
 * @details
 *        Many algorithms have constraints on the input data that it can process and will
 *        need to reshape the data in some way in order to fit its limitations.
 *        This class is to record those modifications (fft_size) that are required that
 *        to ensure consistent results between different implementations
 */
class CommonPlan
{
    public:
        CommonPlan(std::size_t input_data_size);
        CommonPlan(std::size_t input_data_size, std::size_t fft_size);

        /**
         * @brief the size of the input data we would like to process
         */
        std::size_t input_size() const;

        /**
         * @brief the closest fft size that can be performed
         * @details the input data will have to be resized to this value before calling the fft algo
         */
        std::size_t fft_size() const;

        /**
         * @brief compares compatability between different CommonPlan objects
         * @return true if they are compatible, false otherwise
         * @details n.b. this is not exact equality
         */
        bool operator==(CommonPlan const&) const;

        /**
         * @return true if the plan is acceptable to all algorithms, false otherwise
         */
        bool valid() const;

        /**
         * @brief   attempts to find a suitable plan from the added plans with minimum FFT size
         *          that works for selected algorithms
         */
        void calculate();

        /**
         * @brief Add an algorithm plan describing the closest it can get in terms of data input size to
         *        evaluating an Fft of the provided input data
         */
        void add(fft::Plan const&);

    private:
        std::size_t _data_size;
        std::size_t _fft_size;
        bool _valid;
        std::vector<fft::Plan> _plans;
};


} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FFT_COMMONPLAN_H
