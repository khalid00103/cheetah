# Intel OpenCL 1D FFT

## Algorithm

The FFT algorithm implemeted in @ref altera/Fft.h is derived from Intel's 1D off-chip design. It can compute the Fast Fourier Transform of a range of data lengths starting from 64 up to 32 million points. The central F-engine implemented in @ref altera/detail/FftWorker.h processes a complex-to-complex FFT on input data provided in a square matrix format. This central FFT can be called to get forward and inverse FFTs for the data length of even powers of 2. In addition to that, two OpenCL kernels are available to get FFTs of odd powers of 2, but are limited to real-to-complex transforms. OpenCL kernels are defined in the Rabbit library with configuration parameters like minimum FFT size, maximum FFT size, last-stage twiddle factors, etc. When the data available are an odd power of 2, they are divided into even and odd samples and processed separately using a central C2C transform. In R2C transforms, the last-stage kernel uses twiddle factors generated in Rabbit, and only half of the spectrum is saved, as the inputs are real.

## FFT size range

2^7 to 2^25 (odd powers of 2 for R2C) and 2^6 to 2^24 (even powers of 2 for C2C)

## Capabilities

Zero padding and data truncation suitable to FFT type

## Rabbit library

https://gitlab.com/ska-telescope/pss/ska-pss-rabbit
