#include "cheetah/modules/fft/cuda/Fft.cuh"
#include "cheetah/modules/fft/cuda/test/FftTest.h"
#include "cheetah/modules/fft/test_utils/FftTester.h"
#include <memory>


namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace cuda {
namespace test {

/**
 * @details Unit tests for FFT Plan generations for different combinations of FFT type and data length
 */

// R2C forward FFT
TYPED_TEST(FftTest, cuda_fft_r2c_test_1024)
{
    using ComplexT = typename data::ComplexTypeTraits<Cuda,TypeParam>::type;
    typedef data::TimeSeries<Cuda, TypeParam> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cuda, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1024);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 1024);
}

// C2R inverse FFT
TYPED_TEST(FftTest, cuda_fft_c2r_test_1024)
{
    using ComplexT = typename data::ComplexTypeTraits<Cuda,TypeParam>::type;
    typedef data::TimeSeries<Cuda, ComplexT> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cuda, TypeParam>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1024);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 1024);
}

// C2C forward FFT
TYPED_TEST(FftTest, cuda_fft_c2c_fwd_test_1024)
{
    using ComplexT = typename data::ComplexTypeTraits<Cuda,TypeParam>::type;
    typedef data::TimeSeries<Cuda, ComplexT> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cuda, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1024);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 1024);
}

// C2C inverse FFT
TYPED_TEST(FftTest, cuda_fft_c2c_inv_test_1024)
{
    using ComplexT = typename data::ComplexTypeTraits<Cuda,TypeParam>::type;
    typedef data::TimeSeries<Cuda, ComplexT> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cuda, TypeParam>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1024);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 1024);
}


template<typename NumericalT>
struct CudaTraitsBase
    : public fft::test::FftTesterTraits<fft::cuda::Fft, NumericalT>
{
        typedef fft::test::FftTesterTraits<fft::cuda::Fft, NumericalT> BaseT;
        typedef Fft::Architecture Arch;
        typedef typename BaseT::DeviceType DeviceType;

    public:
        template<typename DataType>
        typename DataType::Allocator allocator(panda::PoolResource<Arch>&) {
            return typename DataType::Allocator();
        }
};

template<typename NumericalT>
struct CudaTraits : CudaTraitsBase<NumericalT>
{
};

template<>
struct CudaTraits<float> : CudaTraitsBase<float>
{
    static double accuracy() { return 5.0e-5; }
};

} // namespace test
} // namespace cuda
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits<float>, cuda::test::CudaTraits<double>> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, FftTester, CudaTraitsTypes);

} // namespace test
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
