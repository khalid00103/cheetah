/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/tdas/Config.h"

#include <utility>

namespace ska {
namespace cheetah {
namespace tdas {

Config::Config()
    : utils::Config("tdas")
    , _active(true)
    , _dm_trials_per_task(50)
    , _size(1<<19)
    , _minimum_size(_size/2)
    , _nharmonics(4)
{
    add(_cuda_config);
    add(_acc_gen_config);
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("dm_trials_per_task",boost::program_options::value<std::size_t>(&_dm_trials_per_task)->default_value(20),
       "The number of DM trials to be processed by each asynchronous TDAS task.")
    ("minimum_size",boost::program_options::value<std::size_t>(&_minimum_size)->default_value(1<<19),
       "The minimum number of samples on which Tdas should run.")
    ("size",boost::program_options::value<std::size_t>(&_size)->default_value(1<<23),
       "The desired number of samples on which Tdas should run.")
    ("nharmonics",boost::program_options::value<std::size_t>(&_nharmonics)->default_value(4),
       "The number of harmonic folds to perform. Here a value of 4 "
       "corresponds to 2^4 harmonics being summed in total. ")
    ("active", boost::program_options::value<bool>(&_active)->default_value(true),
        "Active flag for TDAS module.");
}

cuda::Config const& Config::cuda_config() const
{
    return _cuda_config;
}

cuda::Config& Config::cuda_config()
{
    return _cuda_config;
}

AccListGenConfig const& Config::acceleration_list_generator() const
{
    return _acc_gen_config;
}

AccListGenConfig& Config::acceleration_list_generator()
{
    return _acc_gen_config;
}

std::size_t Config::dm_trials_per_task() const
{
    return _dm_trials_per_task;
}

void Config::dm_trials_per_task(std::size_t ntrials)
{
    _dm_trials_per_task = ntrials;
}

std::size_t Config::minimum_size() const
{
    return _minimum_size;
}

void Config::minimum_size(std::size_t minimum_size)
{
    _minimum_size = minimum_size;
}

std::size_t Config::size() const
{
    return _size;
}

void Config::size(std::size_t size)
{
    _size = size;
}

void Config::number_of_harmonic_sums(std::size_t nharmonics)
{
    _nharmonics = nharmonics;
}

std::size_t Config::number_of_harmonic_sums() const
{
    return _nharmonics;
}

bool Config::active() const
{
    return _active;
}

} // namespace tdas
} // namespace cheetah
} // namespace ska
