include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_tdrt_src_cpu
    src/gtest_tdrt.cpp
#    src/Tdrt-TDT-Reference.cpp
)

set(gtest_tdrt_src_cuda)

set_source_files_properties(
    ${EXT_CREFERENCE_OBJ} PROPERTIES
    EXTERNAL_OBJECT TRUE  # Identifies this as an object file
    GENERATED TRUE  # Avoids need for file to exist at configure-time
)

if(ENABLE_CUDA)
    cuda_add_executable(gtest_tdrt ${gtest_tdrt_src_cuda} ${gtest_tdrt_src_cpu})
else()
    add_executable(gtest_tdrt ${gtest_tdrt_src_cpu})
endif()

target_link_libraries(gtest_tdrt ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_tdrt gtest_tdrt)
