/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SIFT_SIFTMODULE_H
#define SKA_CHEETAH_SIFT_SIFTMODULE_H

#include "cheetah/sift/detail/SiftAlgoFactory.h"
#include "cheetah/utils/AlgoModule.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/Scl.h"
#include "panda/ConfigurableTask.h"
#include "panda/PoolSelector.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace sift {

template<typename ConfigT, typename HandlerT>
class SiftTraits {
    public:
        typedef ConfigT ConfigType;
        typedef typename ConfigType::PoolType PoolType;
        typedef HandlerT Handler;
};

template<typename SiftTraits, typename SiftAlgo, typename... SiftAlgos>
class SiftAlgoModuleTraits : public SiftTraits
{
    public:
        typedef SiftAlgoFactory<SiftTraits> AlgoFactory;
        typedef std::tuple<SiftAlgo, SiftAlgos...> Algos;
        typedef std::tuple<panda::SubmitMethod<std::shared_ptr<data::Ccl> const&>
                          > Signatures;
};

/**
 * @brief Module for configuration and launnch of Sift algorithms
 * @details
 */
template<class SiftTraits, typename... SiftAlgos>
class SiftModule : public utils::AlgoModule<SiftAlgoModuleTraits<SiftTraits, SiftAlgos...>>
{
        typedef utils::AlgoModule<SiftAlgoModuleTraits<SiftTraits, SiftAlgos...>> BaseT;
        typedef typename SiftTraits::ConfigType Config;
        typedef typename SiftTraits::Handler Handler;

    public:
        SiftModule(Config const&, Handler& handler);

        /**
         * @brief syncronous call
         */
        template <typename Arch>
        std::shared_ptr<data::Scl> operator()(
                                    panda::PoolResource<Arch>& resource,
                                    data::Ccl& input);

        /**
         * @brief asyncronous call
         */
        std::shared_ptr<panda::ResourceJob> operator()(std::shared_ptr<data::Ccl> const&) const;

        /**
         * @brief asyncronous call for types that can be converted to data:Ccl
         */
        template<typename OtherDataType
                , typename std::enable_if<std::is_convertible<OtherDataType, data::Ccl>::value, bool>::type = true
                >
        std::shared_ptr<panda::ResourceJob> operator()(std::shared_ptr<OtherDataType> const&);

};


} // namespace sift
} // namespace cheetah
} // namespace ska
#include "cheetah/sift/detail/SiftModule.cpp"

#endif // SKA_CHEETAH_SIFT_SIFTMODULE_H
